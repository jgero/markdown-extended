package markdownextended

import (
	"fmt"
	"html"
	"io"
	"strings"
	"text/template"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	mdHtml "github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
)

type tocEntry struct {
	Level   int
	Content string
	Id      string
}

type Result struct {
	Html        string
	Description string
}

type MarkdownConverter interface {
	ConvertMarkdownToHtml(string) (Result, error)
}

type converterWithOptions struct {
	imageLinkPrefix       string
	hasReadFirstParagraph bool
	toc                   []tocEntry
	description           string
}

func NewConverter() MarkdownConverter {
	return &converterWithOptions{
		imageLinkPrefix: "",
	}
}

func NewConverterWithPrefix(imageLinkPrefix string) MarkdownConverter {
	return &converterWithOptions{
		imageLinkPrefix: imageLinkPrefix,
	}
}

func (co *converterWithOptions) ConvertMarkdownToHtml(input string) (Result, error) {
	extensions := parser.CommonExtensions | parser.FencedCode | parser.Footnotes | parser.AutoHeadingIDs
	p := parser.NewWithExtensions(extensions)
	opts := mdHtml.RendererOptions{
		Flags:          mdHtml.CommonFlags,
		RenderNodeHook: co.render,
	}
	renderer := mdHtml.NewRenderer(opts)
	content := string(markdown.ToHTML([]byte(input), p, renderer))
	return Result{Html: content, Description: html.EscapeString(co.description)}, nil
}

func (cwo *converterWithOptions) render(w io.Writer, node ast.Node, entering bool) (ast.WalkStatus, bool) {
	switch typedNode := node.(type) {
	case *ast.Document:
		if entering {
			// reset renderer output when entering document
			cwo.toc = make([]tocEntry, 0)
			cwo.hasReadFirstParagraph = false
			cwo.description = ""
		} else {
			// add toc when leaving the document
			cwo.renderToc(w)
		}
		// default rendering still has to be done
		return ast.GoToNext, false
	case *ast.Link:
		// footnotes and links that are not to external sites will not be rendered here
		if !entering || typedNode.NoteID != 0 || !strings.HasPrefix(string(typedNode.Destination), "http") {
			return ast.GoToNext, false
		}
		cwo.renderExternalLink(w, typedNode)
		// external links are rendered now so return true
		return ast.GoToNext, true
	case *ast.Heading:
		// track headings for the table of contents
		if !entering {
			// only track it on entering
			return ast.GoToNext, false
		}
		cwo.toc = append(cwo.toc, tocEntry{
			Level: typedNode.Level,
			// headings always have exatly one leaf child so this is safe
			Content: string(typedNode.GetChildren()[0].AsLeaf().Literal),
			Id:      typedNode.HeadingID,
		})
		// only tracking was done here so node still has to be rendered
		return ast.GoToNext, false
	case *ast.Paragraph:
		// track first paragraph as description
		if !entering || cwo.hasReadFirstParagraph {
			// only track the first paragraph and only on entering
			return ast.GoToNext, false
		}
		cwo.description = string(typedNode.GetChildren()[0].AsLeaf().Literal)
		cwo.hasReadFirstParagraph = true
		// only tracking was done here so node still has to be rendered
		return ast.GoToNext, false
	case *ast.Image:
		// prefix only links to internal images
		if strings.HasPrefix(string(typedNode.Destination), "http") {
			return ast.GoToNext, false
		}
		out := ""
		if entering {
			out += "<img src=\""
			out += cwo.imageLinkPrefix
			out += string(typedNode.Destination)
			out += "\" alt=\""
			// leave attribute open for rendering of alt text
			io.WriteString(w, out)
		} else {
			// close attribute after writing inner alt text
			out += "\" >"
		}
		io.WriteString(w, out)
		// node rendering was done
		return ast.GoToNext, true
	}
	// nothing is rendered in default case
	return ast.GoToNext, false
}

func (cwo *converterWithOptions) renderToc(w io.Writer) {
	const tocTemplate = `<ul class="table-of-contents">{{range .}}<li><a href="#{{.Id}}" class="level-{{.Level}}">{{.Content}}</a></li>{{end}}</ul>`
	templ := template.Must(template.New("toc").Parse(tocTemplate))
	err := templ.Execute(w, cwo.toc)
	if err != nil {
		// panic here because written output may be unusable
		panic(fmt.Sprintf("executing toc templated failed: %v", err))
	}
}

func (cwo *converterWithOptions) renderExternalLink(w io.Writer, typedNode *ast.Link) {
	out := "<a href=\""
	out += string(typedNode.Destination)
	out += "\" referrerpolicy=\"no_referrer\" target=\"_blank\""
	if len(typedNode.Title) > 0 {
		out += " title=\""
		out += string(typedNode.Title)
		out += "\""
	}
	// other attrs are ignored in this case
	out += ">"
	io.WriteString(w, out)
}
