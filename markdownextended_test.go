package markdownextended_test

import (
	"fmt"
	"strings"
	"testing"

	markdownextended "gitlab.com/jgero/markdown-extended"
)

func TestLinkRendering(t *testing.T) {
	title := "this link is a example"
	link := "https://example.com"
	md := "[" + title + "](" + link + ")"
	conv := markdownextended.NewConverter()
	res, err := conv.ConvertMarkdownToHtml(md)
	if err != nil {
		t.Fatalf("converting should not result in an error: %v", err)
	}
	if !strings.Contains(res.Html, title) {
		t.Errorf("link should contain the provided markdown title but is: %v", res.Html)
	}
	if !strings.Contains(res.Html, link) {
		t.Errorf("link should contain the provided url but is: %v", res.Html)
	}
	if !strings.Contains(res.Html, "target=\"_blank\"") {
		t.Errorf("link should have target _blank to open it in a new tab but is: %v", res.Html)
	}
	if res.Description != "" {
		t.Errorf("markdown without a paragraph should not have a description but has: %v", res.Description)
	}
}

func TestImageLinkRendering(t *testing.T) {
	cases := []struct {
		title          string
		prefix         string
		link           string
		expectedResult string
	}{
		{"nice image", "", "my_nice_image.jpg", "my_nice_image.jpg"},
		{"nice image", "/my/prefix/", "my_nice_image.jpg", "/my/prefix/my_nice_image.jpg"},
		{"nice image", "", "https://example.com/my_nice_image.jpg", "https://example.com/my_nice_image.jpg"},
		{"nice image", "/my/prefix/", "https://example.com/my_nice_image.jpg", "https://example.com/my_nice_image.jpg"},
	}

	for _, c := range cases {
		md := "![" + c.title + "](" + c.link + ")"
		conv := markdownextended.NewConverterWithPrefix(c.prefix)
		res, err := conv.ConvertMarkdownToHtml(md)
		if err != nil {
			t.Fatalf("converting should not result in an error: %v", err)
		}
		if !strings.Contains(res.Html, c.title) {
			t.Errorf("image should contain the provided markdown title")
		}
		expectedUrl := `src="` + c.expectedResult + `"`
		if !strings.Contains(res.Html, expectedUrl) {
			t.Logf("expected '%s' to contain '%s'", res.Html, expectedUrl)
			t.Errorf("image source should be unmodified url")
		}
		expectedAlt := `alt="` + c.title + `"`
		if !strings.Contains(res.Html, expectedAlt) {
			t.Logf("expected '%s' to contain '%s'", res.Html, expectedAlt)
			t.Errorf("image should contain correct alt tag")
		}
	}

}

func TestDescription(t *testing.T) {
	cases := []struct {
		firstPara      string
		secondPara     string
		expectedResult string
	}{
		{"hello this is the first paragraph\n it can have newlines in it", "and this should be the second para", "hello this is the first paragraph\n it can have newlines in it"},
		{"![my image](http://example.com)", "|asdf|asdf|", ""},
	}
	for _, c := range cases {
		md := fmt.Sprintf("# my heading\n\n%s\n\n%s", c.firstPara, c.secondPara)
		conv := markdownextended.NewConverterWithPrefix("/my/prefix/")
		res, err := conv.ConvertMarkdownToHtml(md)
		if err != nil {
			t.Fatalf("converting should not result in an error: %v", err)
		}
		if res.Description != c.expectedResult {
			t.Logf("expected markdown content:\n'%s'\nto have description:\n'%s'\nbut has:\n'%s'", md, c.expectedResult, res.Description)
			t.Errorf("description should be the first paragraph of the markdown content")
		}
	}
}

func TestToc(t *testing.T) {
	titleLiteral := "my title"
	title := "# " + titleLiteral
	lorem := "Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat."
	headingLiteral := "subtitle"
	heading := "## " + headingLiteral
	md := title
	for i := 1; i <= 5; i++ {
		md = fmt.Sprintf("%s\n\n%s %d\n\n%s", md, heading, i, lorem)
	}
	conv := markdownextended.NewConverter()
	res, err := conv.ConvertMarkdownToHtml(md)
	if err != nil {
		t.Fatalf("converting should not result in an error: %v", err)
	}
	for i := 0; i <= 5; i++ {
		var lit string
		var level int
		if i == 0 {
			lit = titleLiteral
			level = 1
		} else {
			lit = fmt.Sprintf("%s %d", headingLiteral, i)
			level = 2
		}
		expected := fmt.Sprintf(`<li><a href="#%s" class="level-%d">%s</a></li>`, strings.ReplaceAll(lit, " ", "-"), level, lit)
		if !strings.Contains(res.Html, expected) {
			t.Logf("expected '%s' to contain '%s'", res.Html, expected)
			t.Errorf("toc should contain all headings")
		}
	}
}
